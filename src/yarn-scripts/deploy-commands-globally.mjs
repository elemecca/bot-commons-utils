import { deployCommands } from '../utils/deployCommands.mjs';

const botRoot = process.argv[2];

deployCommands(botRoot)
  .then(() => {
    console.log('Global commands deployment completed successfully.');
  })
  .catch((error) => {
    console.error('Error during global commands deployment:', error);
  });
