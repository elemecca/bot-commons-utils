import { REST } from 'discord.js';
import { Routes } from 'discord.js';
import { importModulesInDir, loadCommonModules } from './importModulesInDir.mjs';
import { config } from 'dotenv';
import path from 'node:path';

config(); // Load environment variables from .env file

export async function deployCommands(botRoot, guildId = null) {
  // Ensure required environment variables are set
  const requiredEnvVars = ['DISCORD_TOKEN', 'DISCORD_CLIENT_ID'];
  if (guildId) {
    requiredEnvVars.push('GUILD_ID');
  }
  for (const envVar of requiredEnvVars) {
    if (!process.env[envVar]) {
      console.error(`Error: Required environment variable ${envVar} is not set.`);
      process.exit(1);
    }
  }

  if (!botRoot) {
    console.error('Error: Bot root directory not specified.');
    process.exit(1);
  }

  try {
    // Load and register commands
    const commands = [];
    for await (const commandModule of importModulesInDir(path.resolve(botRoot, 'src/commands'), { recursive: true })) {
      commands.push(commandModule.data.toJSON());
    }

    // Load and register common modules
    const commonModules = await loadCommonModules(botRoot);
    for (const commandModule of commonModules.commands) {
      commands.push(commandModule.data.toJSON());
    }

    const rest = new REST({ version: '10' }).setToken(process.env.DISCORD_TOKEN);

    let data;
    if (guildId) {
      data = await rest.put(
        Routes.applicationGuildCommands(process.env.DISCORD_CLIENT_ID, guildId),
        { body: commands }
      );
    } else {
      data = await rest.put(
        Routes.applicationCommands(process.env.DISCORD_CLIENT_ID),
        { body: commands }
      );
    }

    console.log(`Successfully registered ${data.length} application commands.`);
  } catch (error) {
    console.error('Error registering application commands:', error);
    process.exit(1);
  }
}
