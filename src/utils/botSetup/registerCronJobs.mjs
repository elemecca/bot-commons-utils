import {importModulesInDir} from '../importModulesInDir.mjs';
import path from 'node:path';
import cron from 'node-cron';

export async function registerCronJobs(bot, botRoot, folder='src/cron') {
  for await (const cronJob of importModulesInDir(path.resolve(botRoot, folder))) {
    // Schedule the cron job
    cron.schedule(cronJob.default.schedule, () => cronJob.default.task(bot.client));
  }
}
