import {importModulesInDir, loadCommonModules} from '../importModulesInDir.mjs';
import {Collection} from 'discord.js';
import path from 'node:path';

export async function registerCommands(bot, botRoot, folder='src/commands') {
  const commonModules = await loadCommonModules(botRoot);

  bot.client.commands = new Collection();
  for await (const module of importModulesInDir(path.resolve(botRoot, folder), { recursive: true })) {
    // Set a new item in the Collection
    // With the key as the command name and the value as the exported module
    bot.client.commands.set(module.data.name, module);
  }
  for (const commandModule of commonModules.commands) {
    bot.client.commands.set(commandModule.data.name, commandModule);
  }
}
