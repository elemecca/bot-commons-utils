export async function loginBot(bot) {
  await bot.client.login(process.env.DISCORD_TOKEN);
  bot.guild = bot.client.guilds.cache.get(process.env.GUILD_ID);
}
