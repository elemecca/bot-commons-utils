export function setupGracefulShutdown(bot) {
  const destroy = () => {
    if (bot.client.roleManager) {
      bot.client.roleManager.teardown();
    }
    bot.client.destroy();
  };
  process.on("SIGINT", destroy);
  process.on("SIGTERM", destroy);
}
