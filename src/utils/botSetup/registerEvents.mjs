import { importModulesInDir, loadCommonModules } from '../importModulesInDir.mjs';
import path from 'node:path';

/**
 * Registers events to the bot client.
 * @param {Object} bot - The bot object.
 * @param {string} botRoot - The root directory of the bot.
 * @param {string} [folder='src/events'] - The folder containing event modules.
 */
export async function registerEvents(bot, botRoot, folder='src/events') {
  // Load common modules including events
  const commonModules = await loadCommonModules(botRoot);

  // Register events from the common modules
  for (const eventModule of commonModules.events) {
    if (eventModule.default.once) {
      bot.client.once(eventModule.default.name, (...args) => eventModule.default.execute(...args));
    } else {
      bot.client.on(eventModule.default.name, (...args) => eventModule.default.execute(...args));
    }
  }

  // Register events from the specified folder
  for await (const event of importModulesInDir(path.resolve(botRoot, folder))) {
    if (event.default.once) {
      bot.client.once(event.default.name, (...args) => event.default.execute(...args));
    } else {
      bot.client.on(event.default.name, (...args) => event.default.execute(...args));
    }
  }
}
