/**
 * This file is just to bring a bunch of setup commands together.
 * It was originally written all as one, but due to differing bot needs,
 * it's been split. However, several bots still just use this one file.
 */
import {registerCommands} from './botSetup/registerCommands.mjs';
import {loginBot} from './botSetup/loginBot.mjs';
import {setupGracefulShutdown} from './botSetup/setupGracefulShutdown.mjs';
import {firstSteps} from './botSetup/firstSteps.mjs';
import {registerEvents} from './botSetup/registerEvents.mjs';
import {registerScripts} from './botSetup/registerScripts.mjs';
import {registerCronJobs} from './botSetup/registerCronJobs.mjs';

export {
  firstSteps,
  registerCommands,
  registerEvents,
  registerScripts,
  registerCronJobs,
  loginBot,
  setupGracefulShutdown
};
